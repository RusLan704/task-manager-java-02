# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: RUSLAN BAKHTIYAROV

**E-MAIL:** rusya.vay@mail.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager-01.jar
```

Скриншот работы jar-файла: https://priscree.ru/img/31ca4369eea192.jpg